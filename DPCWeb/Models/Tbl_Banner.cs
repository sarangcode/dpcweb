//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DPCWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Banner
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public string ImgSrc { get; set; }
        public Nullable<int> Priority { get; set; }
    }
}
