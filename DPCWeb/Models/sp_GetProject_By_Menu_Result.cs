//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DPCWeb.Models
{
    using System;
    
    public partial class sp_GetProject_By_Menu_Result
    {
        public int Project_Id { get; set; }
        public string Project_Name { get; set; }
        public string Project_Imgsrc { get; set; }
        public string Project_Status { get; set; }
    }
}
