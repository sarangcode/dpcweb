﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DPCWeb.Models
{
    public class EntityProjectImg
    {
        private int project_Id;

        public int Project_Id
        {
            get { return project_Id; }
            set { project_Id = value; }
        }
        private string project_Name;

        public string Project_Name
        {
            get { return project_Name; }
            set { project_Name = value; }
        }
        private string project_Img;

        public string Project_Img
        {
            get { return project_Img; }
            set { project_Img = value; }
        }
        private string project_Status;

        public string Project_Status
        {
            get { return project_Status; }
            set { project_Status = value; }
        }
    }
}