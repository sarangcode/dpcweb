//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DPCWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Project_Img
    {
        public int Project_Img_Id { get; set; }
        public Nullable<int> Project_Id { get; set; }
        public string Project_Imgsrc { get; set; }
    
        public virtual Tbl_Project Tbl_Project { get; set; }
    }
}
