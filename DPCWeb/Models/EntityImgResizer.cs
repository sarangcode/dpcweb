﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DPCWeb.Models
{
    public class EntityImgResizer
    {
        private int newWidth;

        public int NewWidth
        {
            get { return newWidth; }
            set { newWidth = value; }
        }

        private int newHeight;

        public int NewHeight
        {
            get { return newHeight; }
            set { newHeight = value; }
        }
        private string stPhotoPath;

        public string StPhotoPath
        {
            get { return stPhotoPath; }
            set { stPhotoPath = value; }
        }
    }
}