﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DPCWeb.Models
{
    public class EntityImage
    {
        private HttpPostedFileBase imgsrc;

        public HttpPostedFileBase Imgsrc
        {
            get { return imgsrc; }
            set { imgsrc = value; }
        }
        private int width;

        public int Width
        {
            get { return width; }
            set { width = value; }
        }
        private int height;

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
        private string savePath;

        public string SavePath
        {
            get { return savePath; }
            set { savePath = value; }
        }
        private string fullPath;

        public string FullPath
        {
            get { return fullPath; }
            set { fullPath = value; }
        }
    }
}