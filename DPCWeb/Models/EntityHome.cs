﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DPCWeb.Models
{
    public class EntityHome
    {
        private IEnumerable<Tbl_Banner> banner;

        public IEnumerable<Tbl_Banner> Banner
        {
            get { return banner; }
            set { banner = value; }
        }

        private IEnumerable<tbl_Services> services;

        public IEnumerable<tbl_Services> Services
        {
            get { return services; }
            set { services = value; }
        }
    }
}