﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DPCWeb.Models;

namespace DPCWeb.Models
{
    public class EntityProject
    {
        dpcpEntities obj = new dpcpEntities();
        private IEnumerable<Tbl_Project> project;

        public IEnumerable<Tbl_Project> Project
        {
            get { return project; }
            set { project = value; }
        }
        private IEnumerable<Tbl_Project_Img> projectimages;

        public IEnumerable<Tbl_Project_Img> Projectimages
        {
            get { return projectimages; }
            set { projectimages = value; }
        }

        private IEnumerable<sp_GetProject_By_Menu_Result> project_WithImages;

        public IEnumerable<sp_GetProject_By_Menu_Result> Project_WithImages
        {
            get { return project_WithImages; }
            set { project_WithImages = value; }
        }       
    }
}