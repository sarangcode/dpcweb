﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DPCWeb.Models
{
    public class EntityDashboard
    {
        private IEnumerable<sp_GetProject_By_Menu_Result> tbl_Project_Img;

        public IEnumerable<sp_GetProject_By_Menu_Result> Tbl_Project_Img
        {
            get { return tbl_Project_Img; }
            set { tbl_Project_Img = value; }
        }
        private IEnumerable<Tbl_Banner> tbl_Banner;

        public IEnumerable<Tbl_Banner> Tbl_Banner
        {
            get { return tbl_Banner; }
            set { tbl_Banner = value; }
        }
        private IEnumerable<Tbl_Employee> tbl_Employee;

        public IEnumerable<Tbl_Employee> Tbl_Employee
        {
            get { return tbl_Employee; }
            set { tbl_Employee = value; }
        }
    }
}