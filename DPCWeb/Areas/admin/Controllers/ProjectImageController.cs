﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;
using System.Text.RegularExpressions;
using DPCWeb.services;

namespace DPCWeb.Areas.admin.Controllers
{
    public class ProjectImageController : Controller
    {
        private dpcpEntities db = new dpcpEntities();

        EntityImage entityimg;
        ServiceImage serviceimg;
        // GET: admin/ProjectImage
        public ActionResult Index()
        {
            var tbl_Project_Img = db.Tbl_Project_Img.Include(t => t.Tbl_Project);
            return View(tbl_Project_Img.ToList());
        }

        // GET: admin/ProjectImage/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Project_Img tbl_Project_Img = db.Tbl_Project_Img.Find(id);
            if (tbl_Project_Img == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Project_Img);
        }

        // GET: admin/ProjectImage/Create
        public ActionResult Create()
        {
            ViewBag.Project_Id = new SelectList(db.Tbl_Project, "Project_Id", "Project_Name");
            return View();
        }

        // POST: admin/ProjectImage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tbl_Project_Img obj, HttpPostedFileBase[] imgfile)
        {
            try
            {
                // TODO: Add insert logic here
                var menu = (from n in db.Tbl_Menus join p in db.Tbl_Project on n.menu_Id equals p.menu_Id where p.Project_Id == obj.Project_Id select n.menu_Name).FirstOrDefault();
                
                Tbl_Project_Img te = new Tbl_Project_Img();

                foreach (var item in imgfile)
                {
                    
                    if (item != null)
                    {

                        entityimg = new EntityImage();
                        serviceimg = new ServiceImage();

                        entityimg.Imgsrc = item;
                        entityimg.Width = 800;
                        entityimg.Height = 533;
                        
                     
                        if (serviceimg.CheckImgDimension(entityimg))
                        {
                      
                            if (menu == "Architecture")
                            {
                                entityimg.SavePath = "/Content/images/Architecture/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }
                            else if (menu == "Compitition")
                            {
                                entityimg.SavePath = "/Content/images/Compitition/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }
                            else if (menu == "Urban")
                            {
                                entityimg.SavePath = "/Content/images/Urban/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }
                            else if (menu == "Town Planning")
                            {
                                entityimg.SavePath = "/Content/images/TownPlanning/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }

                            te.Project_Id = obj.Project_Id;
                            db.Tbl_Project_Img.Add(te);
                            db.SaveChanges(); 

                        }
                        else
                        {
                            ViewBag.imgerror = "Image Dimension should be 800*533 Pixel";
                            ViewBag.Project_Id = new SelectList(db.Tbl_Project, "Project_Id", "Project_Name");
                            return View();
                        }
                    }
                    
                }
                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.Project_Id = new SelectList(db.Tbl_Project, "Project_Id", "Project_Name");
                return View();
            }
        }

        // GET: admin/ProjectImage/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Project_Img tbl_Project_Img = db.Tbl_Project_Img.Find(id);
            if (tbl_Project_Img == null)
            {
                return HttpNotFound();
            }
            ViewBag.Project_Id = new SelectList(db.Tbl_Project, "Project_Id", "Project_Name", tbl_Project_Img.Project_Id);
            return View(tbl_Project_Img);
        }

        // POST: admin/ProjectImage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tbl_Project_Img obj, FormCollection fc, HttpPostedFileBase imgfile)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add update logic here

                    var menu = (from n in db.Tbl_Menus join p in db.Tbl_Project on n.menu_Id equals p.menu_Id where p.Project_Id == obj.Project_Id select n.menu_Name).FirstOrDefault();

                    var te = db.Tbl_Project_Img.Find(obj.Project_Img_Id);
                    te.Project_Id = obj.Project_Id;

                    if (imgfile != null)
                    {
                        entityimg = new EntityImage();
                        serviceimg = new ServiceImage();

                        entityimg.Imgsrc = imgfile;
                        entityimg.Width = 800;
                        entityimg.Height = 533;


                        if (serviceimg.CheckImgDimension(entityimg))
                        {
                            //delete img file---------------------------------------------------
                            serviceimg = new ServiceImage();
                            serviceimg.DeleteProjectImage(obj);
                            //----------------------------------------------------------------------------------
                            if (menu == "Architecture")
                            {
                                entityimg.SavePath = "/Content/images/Architecture/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }
                            else if (menu == "Compitition")
                            {
                                entityimg.SavePath = "/Content/images/Compitition/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }
                            else if (menu == "Urban")
                            {
                                entityimg.SavePath = "/Content/images/Urban/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }
                            else if (menu == "Town Planning")
                            {
                                entityimg.SavePath = "/Content/images/TownPlanning/";
                                te.Project_Imgsrc = serviceimg.AddProjectImages(entityimg);
                            }

                            te.Project_Id = obj.Project_Id;
                            

                        }
                        else
                        {
                
                            ViewBag.imgerror = "Image Dimension should be 800*533 Pixel";
                            ViewBag.Project_Id = new SelectList(db.Tbl_Project, "Project_Id", "Project_Name", obj.Project_Id);
                            return View(obj);
                        }

                    }
                    else
                    {
                        string strpath = fc["profile"];
                        te.Project_Imgsrc = strpath;

                    }
                    db.SaveChanges();

                    
                }
                catch
                {
                    return View();
                }
            }
            ViewBag.Project_Id = new SelectList(db.Tbl_Project, "Project_Id", "Project_Name", obj.Project_Id);
            return RedirectToAction("Index");
        }

        // GET: admin/ProjectImage/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Project_Img tbl_Project_Img = db.Tbl_Project_Img.Find(id);
            if (tbl_Project_Img == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Project_Img);
        }

        // POST: admin/ProjectImage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //delete img file---------------------------------------------------
            string deldbpath = db.Tbl_Project_Img.Where(x => x.Project_Img_Id == id).Select(s => s.Project_Imgsrc).FirstOrDefault();
            if (System.IO.File.Exists(Server.MapPath(@"~" + deldbpath + "")))
            {
                System.IO.File.Delete(Server.MapPath(@"~" + deldbpath + ""));
            }
            //----------------------------------------------------------------------------------

            Tbl_Project_Img tbl_Project_Img = db.Tbl_Project_Img.Find(id);
            db.Tbl_Project_Img.Remove(tbl_Project_Img);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
