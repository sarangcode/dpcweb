﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;
using DPCWeb.services;

namespace DPCWeb.Areas.admin.Controllers
{
    public class BannerController : Controller
    {
        private dpcpEntities db = new dpcpEntities();

        EntityImage entityimg;
        ServiceImage serviceimg;
        // GET: admin/Banner
        public ActionResult Index()
        {
            return View(db.Tbl_Banner.ToList());
        }

        // GET: admin/Banner/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Banner tbl_Banner = db.Tbl_Banner.Find(id);
            if (tbl_Banner == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Banner);
        }

        // GET: admin/Banner/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin/Banner/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Discription,ImgSrc,Priority")] Tbl_Banner obj, HttpPostedFileBase imgfile)
        {
            if (ModelState.IsValid)
            {
                Tbl_Banner te = new Tbl_Banner();
               
                if (imgfile != null)
                {
                    entityimg = new EntityImage();
                    entityimg.Imgsrc = imgfile;
                    entityimg.Width = 1680;
                    entityimg.Height = 900;

                    serviceimg = new ServiceImage();
                    if (serviceimg.CheckImgDimension(entityimg))
                    {

                        entityimg.SavePath = "/Content/images/Banner/";
                        te.ImgSrc = serviceimg.AddImages(entityimg);

                        te.Name = obj.Name;
                        te.Priority = obj.Priority;
                        te.Discription = obj.Discription;

                        db.Tbl_Banner.Add(te);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        @ViewBag.imgerror = "Image Dimension should be 1680*900 Pixel";

                    }


                }
                
                
            }
            return View(obj);
            
        }

        // GET: admin/Banner/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Banner tbl_Banner = db.Tbl_Banner.Find(id);
            if (tbl_Banner == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Banner);
        }

        // POST: admin/Banner/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tbl_Banner obj, HttpPostedFileBase imgfile,FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var te = db.Tbl_Banner.Find(obj.Id);
                te.Name = obj.Name;
                te.Discription = obj.Discription;
                te.Priority = obj.Priority;

                if (imgfile != null)
                {
                    entityimg = new EntityImage();
                    entityimg.Imgsrc = imgfile;
                    entityimg.Width = 1680;
                    entityimg.Height = 900;

                    serviceimg = new ServiceImage();
                    if (serviceimg.CheckImgDimension(entityimg))
                    {

                        entityimg.SavePath = "/Content/images/Banner/";
                        te.ImgSrc = serviceimg.AddImages(entityimg);

                        db.Entry(te).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                     
                    }
                }
                else
                {
                    string strpath = fc["profile"];
                    te.ImgSrc = strpath;

                    db.Entry(te).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }

                
            }
            ViewBag.imgerror = "Image Dimension should be 1680*900 Pixel";
            return View(obj);
        }

        // GET: admin/Banner/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Banner tbl_Banner = db.Tbl_Banner.Find(id);
            if (tbl_Banner == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Banner);
        }

        // POST: admin/Banner/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tbl_Banner tbl_Banner = db.Tbl_Banner.Find(id);
            db.Tbl_Banner.Remove(tbl_Banner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
