﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;

namespace DPCWeb.Areas.admin.Controllers
{
    public class EmployeeController : Controller
    {
        private dpcpEntities db = new dpcpEntities();

        // GET: admin/Employee
        public ActionResult Index()
        {
            var tbl_Employee = db.Tbl_Employee.Include(t => t.Tbl_Department);
            return View(tbl_Employee.ToList());
        }

        // GET: admin/Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Employee tbl_Employee = db.Tbl_Employee.Find(id);
            if (tbl_Employee == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Employee);
        }

        // GET: admin/Employee/Create
        public ActionResult Create()
        {
            ViewBag.Dept_Id = new SelectList(db.Tbl_Department, "Dept_Id", "Dept_Name");
            return View();
        }

        // POST: admin/Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tbl_Employee obj, HttpPostedFileBase imgfile)
        {
            try
            {
                // TODO: Add insert logic here

                Tbl_Employee te = new Tbl_Employee();

                if (imgfile != null)
                {
                    string pic = System.IO.Path.GetFileName(imgfile.FileName);
                    string path = System.IO.Path.Combine(Server.MapPath("~/Content/images/employees"), pic);
                    // file is uploaded
                    imgfile.SaveAs(path);

                    string fullpath = "/Content/images/employees/" + imgfile.FileName;

                    te.Emp_Img = fullpath;

                }
                te.Biometric_Id = obj.Biometric_Id;
                te.Emp_Designation = obj.Emp_Designation;
                te.Emp_Id = obj.Emp_Id;
                te.Emp_Name = obj.Emp_Name;
                te.Dept_Id = obj.Dept_Id;
                te.Priority = obj.Priority;
                te.Description = obj.Description;

                ViewBag.Dept_Id = new SelectList(db.Tbl_Department, "Dept_Id", "Dept_Name", te.Dept_Id);
                db.Tbl_Employee.Add(te);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: admin/Employee/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Employee tbl_Employee = db.Tbl_Employee.Find(id);
            if (tbl_Employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.Dept_Id = new SelectList(db.Tbl_Department, "Dept_Id", "Dept_Name", tbl_Employee.Dept_Id);
            return View(tbl_Employee);
        }

        // POST: admin/Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tbl_Employee obj, FormCollection fc, HttpPostedFileBase imgfile)
        {
            try
            {
                // TODO: Add update logic here

                var te = db.Tbl_Employee.Find(obj.Emp_Id);
                te.Emp_Name = obj.Emp_Name;
                te.Emp_Designation = obj.Emp_Designation;
                te.Emp_Img = obj.Emp_Img;
                te.Biometric_Id = obj.Biometric_Id;
                te.Dept_Id = obj.Dept_Id;
                te.Priority = obj.Priority;
                te.Description = obj.Description;
                ViewBag.Dept_Id = new SelectList(db.Tbl_Department, "Dept_Id", "Dept_Name", te.Dept_Id);

                if (imgfile != null)
                {

                    string pic = System.IO.Path.GetFileName(imgfile.FileName);
                    string path = System.IO.Path.Combine(Server.MapPath("~/Content/images/employees"), pic);
                    // file is uploaded
                    imgfile.SaveAs(path);

                    string fullpath = "/Content/images/employees/" + imgfile.FileName;

                    te.Emp_Img = fullpath;
                }
                else
                {
                    string strpath = fc["profile"];
                    te.Emp_Img = strpath;

                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: admin/Employee/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Employee tbl_Employee = db.Tbl_Employee.Find(id);
            if (tbl_Employee == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Employee);
        }

        // POST: admin/Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tbl_Employee tbl_Employee = db.Tbl_Employee.Find(id);
            db.Tbl_Employee.Remove(tbl_Employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
