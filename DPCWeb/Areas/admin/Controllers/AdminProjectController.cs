﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;

namespace DPCWeb.Areas.admin.Controllers
{
    public class AdminProjectController : Controller
    {
        private dpcpEntities db = new dpcpEntities();

        // GET: admin/AdminProject
        public ActionResult Index()
        {
            var tbl_Project = db.Tbl_Project.Include(t => t.Tbl_Menus);
            return View(tbl_Project.ToList());
        }

        // GET: admin/AdminProject/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Project tbl_Project = db.Tbl_Project.Find(id);
            if (tbl_Project == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Project);
        }

        // GET: admin/AdminProject/Create
        public ActionResult Create()
        {
            ViewBag.menu_Id = new SelectList(db.Tbl_Menus, "menu_Id", "menu_Name");
            return View();
        }

        // POST: admin/AdminProject/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Project_Id,menu_Id,Project_Name,Project_Type,Project_Status,Priority")] Tbl_Project tbl_Project)
        {
            if (ModelState.IsValid)
            {
                db.Tbl_Project.Add(tbl_Project);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.menu_Id = new SelectList(db.Tbl_Menus, "menu_Id", "menu_Name", tbl_Project.menu_Id);
            return View(tbl_Project);
        }

        // GET: admin/AdminProject/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tbl_Project tbl_Project = db.Tbl_Project.Find(id);
            if (tbl_Project == null)
            {
                return HttpNotFound();
            }
            ViewBag.menu_Id = new SelectList(db.Tbl_Menus, "menu_Id", "menu_Name", tbl_Project.menu_Id);
            return View(tbl_Project);
        }

        // POST: admin/AdminProject/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Project_Id,menu_Id,Project_Name,Project_Type,Project_Status,Priority")] Tbl_Project tbl_Project)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Project).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.menu_Id = new SelectList(db.Tbl_Menus, "menu_Id", "menu_Name", tbl_Project.menu_Id);
            return View(tbl_Project);
        }

        // GET: admin/AdminProject/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int proimgcnt = db.Tbl_Project_Img.Where(s => s.Project_Id == id).Count();
            if (proimgcnt > 0)
            {
                ViewBag.ermsg = "You should delete all the images related to project then try to delete a project..";
                //return RedirectToAction("Delete", id);
            }

            Tbl_Project tbl_Project = db.Tbl_Project.Find(id);
            if (tbl_Project == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Project);
        }

        // POST: admin/AdminProject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            int proimgcnt = db.Tbl_Project_Img.Where(s => s.Project_Id == id).Count();
            if (proimgcnt > 0)
            {
                ViewBag.ermsg = "You should delete all the images related to project then try to delete a project..";
                return RedirectToAction("Delete", id);
               
            }
            else
            {
                Tbl_Project tbl_Project = db.Tbl_Project.Find(id);
                db.Tbl_Project.Remove(tbl_Project);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
