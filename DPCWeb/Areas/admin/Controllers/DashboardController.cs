﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;
using DPCWeb.Repository;

namespace DPCWeb.Areas.admin.Controllers
{
    public class DashboardController : Controller
    {
        dpcpEntities db = new dpcpEntities();
        EntityDashboard EntityDash;


         IProject objrepo;

        EntityProject objEntity;
        public DashboardController()
        {
            objrepo = new RepoProject(new dpcpEntities());
        }

        // GET: admin/Dashboard
        public ActionResult Index()
        {
            EntityDash = new EntityDashboard();

            EntityDash.Tbl_Project_Img = objrepo.getAll_ImagebyId(0);
            EntityDash.Tbl_Banner = db.Tbl_Banner;
            EntityDash.Tbl_Employee = db.Tbl_Employee;
            return View(EntityDash);
        }
    }
}