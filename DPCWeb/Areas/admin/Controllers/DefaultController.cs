﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPCWeb.Areas.admin.Controllers
{
    public class DefaultController : Controller
    {
        // GET: admin/Default
        public ActionResult Index()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Index", "Home", new { area = "" });
        }
    }
}