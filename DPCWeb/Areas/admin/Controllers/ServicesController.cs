﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;

namespace DPCWeb.Areas.admin.Controllers
{
    public class ServicesController : Controller
    {
        private dpcpEntities db = new dpcpEntities();

        // GET: admin/Services
        public ActionResult Index()
        {
            return View(db.tbl_Services.ToList());
        }

        // GET: admin/Services/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Services tbl_Services = db.tbl_Services.Find(id);
            if (tbl_Services == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Services);
        }

        // GET: admin/Services/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin/Services/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,service,fa_fa_class,priority")] tbl_Services tbl_Services)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Services.Add(tbl_Services);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_Services);
        }

        // GET: admin/Services/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Services tbl_Services = db.tbl_Services.Find(id);
            if (tbl_Services == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Services);
        }

        // POST: admin/Services/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,service,fa_fa_class,priority")] tbl_Services tbl_Services)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Services).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Services);
        }

        // GET: admin/Services/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Services tbl_Services = db.tbl_Services.Find(id);
            if (tbl_Services == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Services);
        }

        // POST: admin/Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Services tbl_Services = db.tbl_Services.Find(id);
            db.tbl_Services.Remove(tbl_Services);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
