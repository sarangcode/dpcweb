﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DPCWeb.Models;
using System.IO;

namespace DPCWeb.services
{
    public class ServiceImage
    {
        dpcpEntities db = new dpcpEntities();
        public bool CheckImgDimension(EntityImage obj)
        {
            var img = System.Drawing.Image.FromStream(obj.Imgsrc.InputStream, true, true);
            int width = img.Width;
            int height = img.Height;
            string extension = Path.GetExtension(obj.Imgsrc.FileName);
            if(width == obj.Width && height == obj.Height && (extension == ".jpg" || extension == ".jpeg"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string AddImages(EntityImage obj)
        {
            var banId = (from a in db.Tbl_Banner orderby a.Id descending select a.Id).FirstOrDefault() + 1;
            string pic = System.IO.Path.GetFileName(obj.Imgsrc.FileName);
            string path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~" + obj.SavePath + ""), (banId+pic));
            // file is uploaded
            obj.Imgsrc.SaveAs(path);

            string fullpath = obj.SavePath + (banId + obj.Imgsrc.FileName);

            return fullpath;
        }


        public string AddProjectImages(EntityImage obj)
        {
            var banId = (from a in db.Tbl_Project_Img orderby a.Project_Img_Id descending select a.Project_Img_Id).FirstOrDefault() + 1;
            string pic = System.IO.Path.GetFileName(obj.Imgsrc.FileName);
            string path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~" + obj.SavePath + ""), (banId + pic));
            // file is uploaded
            obj.Imgsrc.SaveAs(path);

            string fullpath = obj.SavePath + (banId + obj.Imgsrc.FileName);

            return fullpath;
        }

        public void DeleteProjectImage(Tbl_Project_Img obj)
        {
            string deldbpath = db.Tbl_Project_Img.Where(x => x.Project_Img_Id == obj.Project_Img_Id).Select(s => s.Project_Imgsrc).FirstOrDefault();
            if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~" + deldbpath + "")))
            {
                System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath(@"~" + deldbpath + ""));
            }
        }
    }
}