﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;
using DPCWeb.Repository;

namespace DPCWeb.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
       IHome objrepo;

        EntityHome objEntity;
        public HomeController()
        {
            objrepo = new RepoHome(new dpcpEntities());
        }
        public ActionResult Index()
        {
            objEntity = new EntityHome();

            objEntity.Banner = objrepo.getallBanner();
            objEntity.Services = objrepo.getallServices();
            return View(objEntity);
        }

    }
}
