﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Repository;
using DPCWeb.Models;


namespace DPCWeb.Controllers
{
    public class ProjectController : Controller
    {
        //
        // GET: /Project/

        //RepoProject objRepo;

        //RepoProject objrepo;

        IProject objrepo;

        EntityProject objEntity;
        public ProjectController()
        {
            objrepo = new RepoProject(new dpcpEntities());
        }
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpGet]
        public ActionResult Architecture()
        {
            objEntity = new EntityProject();

            objEntity.Project = objrepo.getProjectName("Architecture");
            objEntity.Projectimages = objrepo.getAll_ImageByMenu("Architecture");
            objEntity.Project_WithImages = objrepo.getproject_WithImagesBy_Menu("Architecture");

            ViewData["projectimg"] = objEntity.Projectimages;  
           
            return View(objEntity);
        }

        public ActionResult UrbanDesign()
        {
            objEntity = new EntityProject();

            objEntity.Project = objrepo.getProjectName("Urban");
            objEntity.Projectimages = objrepo.getAll_ImageByMenu("Urban");
            objEntity.Project_WithImages = objrepo.getproject_WithImagesBy_Menu("Urban");

            return View(objEntity);
            
        }

        public ActionResult TownPlanning()
        {
            objEntity = new EntityProject();

            objEntity.Project = objrepo.getProjectName("Town Planning");
            objEntity.Projectimages = objrepo.getAll_ImageByMenu("Town Planning");
            objEntity.Project_WithImages = objrepo.getproject_WithImagesBy_Menu("Town Planning");

            return View(objEntity);
        }

        public ActionResult Compitition()
        {
            objEntity = new EntityProject();

            objEntity.Project = objrepo.getProjectName("Compitition");
            objEntity.Projectimages = objrepo.getAll_ImageByMenu("Compitition");
            objEntity.Project_WithImages = objrepo.getproject_WithImagesBy_Menu("Compitition");

            return View(objEntity);
        }

        [HttpPost]
      
        public ActionResult Architecture(int id)
        {
            
            objEntity = new EntityProject();

            objEntity.Project = objrepo.getProjectName("Architecture");
            objEntity.Projectimages = objrepo.getAll_ImageByMenu("Architecture").Where(x => x.Project_Id == id);
            objEntity.Project_WithImages = objrepo.getproject_WithImagesBy_Menu("Architecture");


            ViewData.Clear();
            ViewData["projectimg"] = objEntity.Projectimages;  
            return View(objEntity);
          
          
        }

       public ActionResult Update(int id)
        {

            objEntity = new EntityProject();

            objEntity.Project = objrepo.getProjectName("Architecture");
            objEntity.Projectimages = objrepo.getAll_ImageByMenu("Architecture").Where(x => x.Project_Id == id);
            objEntity.Project_WithImages = objrepo.getproject_WithImagesBy_Menu("Architecture");

            return View("Architecture", objEntity);
           
        }

        public ActionResult viewdata()
        {
            return View();
        }

    }
}



