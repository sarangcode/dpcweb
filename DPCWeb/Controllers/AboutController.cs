﻿using DPCWeb.Models;
using DPCWeb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPCWeb.Controllers
{
    public class AboutController : Controller
    {
        //
        // GET: /About/
        IAward objrepo;

       
        public AboutController()
        {
            objrepo = new RepoAward(new dpcpEntities());
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Careers()
        {
            return View();
        }

        public ActionResult Award()
        {
            var award = objrepo.get_All_AwardImg();
            return View(award);
        }
    }
}
