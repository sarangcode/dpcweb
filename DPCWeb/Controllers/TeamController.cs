﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Repository;
using DPCWeb.Models;

namespace DPCWeb.Controllers
{
    public class TeamController : Controller
    {
        //
        // GET: /Team/
         IEmployee Iobj;
         dpcpEntities dc = new dpcpEntities();

        public TeamController()
         {
             Iobj = new RepoEmployee(new dpcpEntities());
         }

        public ActionResult Index()
         {
             ViewBag.deptNo = dc.Sp_GetDeptCount().ToList();
             var p = dc.Sp_GetDeptCount().Select(x => x.dept_id).ToList();
             var Emp = Iobj.get_All_Employee().ToList();
             return View(Emp);
            
        }

    }
}
