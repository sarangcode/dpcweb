﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DPCWeb.Models;

namespace DPCWeb.Controllers
{
    public class AdminController : Controller
    {
        dpcpEntities db = new dpcpEntities();
        // GET: Admin
        public ActionResult Index()
        
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc,AdminLogin obj)
        {
            int usercnt = db.AdminLogins.Where(x => x.Admin_Name == obj.Admin_Name && x.Admin_Password == obj.Admin_Password).Count();
            if (usercnt > 0)
            {
                Session["admin"] = obj.Admin_Name;
                return Redirect("admin/Dashboard");
            }
            else
            {
                ViewBag.ermsg = "User Id & Password must be correct..";
                return View();
            }
        }

      
    }
}