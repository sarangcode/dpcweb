﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPCWeb.Models;

namespace DPCWeb.Repository
{
    interface IEmployee:IDisposable
    {
        IEnumerable<Tbl_Employee> get_All_Employee();
    }
}
