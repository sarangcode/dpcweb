﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPCWeb.Models;

namespace DPCWeb.Repository
{
    interface IProject:IDisposable
    {
        IEnumerable<Tbl_Project> getProjectName(string menu);

        int get_Id_ByMenu(string menu);

        IEnumerable<sp_GetProject_By_Menu_Result> getAll_ImagebyId(int id);

        IEnumerable<Tbl_Project_Img> getAll_Image();

        IEnumerable<Tbl_Project_Img> getAll_ImageByMenu(string menu);

        IEnumerable<sp_GetProject_By_Menu_Result> getproject_WithImagesBy_Menu(string menu);

        void save();
    }
}
