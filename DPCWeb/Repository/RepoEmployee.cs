﻿using DPCWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DPCWeb.Repository
{
    public class RepoEmployee:IEmployee
    {
        dpcpEntities dc;
        public RepoEmployee(dpcpEntities obj)
        {
            this.dc = obj;
        }
        public void Dispose()
        {
            //Win32.DestroyHandle(this.CursorFileBitmapIconServiceHandle);
        }
        public IEnumerable<Tbl_Employee> get_All_Employee()
        {
            var emp = dc.Tbl_Employee.OrderBy(s => s.Priority).ToList();
            return emp;
        }
    }
}