﻿using DPCWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DPCWeb.Repository
{
    public class RepoHome:IHome
    {
         dpcpEntities dc;


         public RepoHome(dpcpEntities obj)
        {
            this.dc = obj;
        }
        public void Dispose()
        {
            //Win32.DestroyHandle(this.CursorFileBitmapIconServiceHandle);
        }

        public IEnumerable<Tbl_Banner> getallBanner()
        {
            var banner = (from n in dc.Tbl_Banner orderby n.Priority select n).ToList();
            return banner;
        }

        public IEnumerable<tbl_Services> getallServices()
        {
            var service = (from n in dc.tbl_Services orderby n.priority select n).ToList();
            return service;
        }
    }
}