﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DPCWeb.Models;

namespace DPCWeb.Repository
{
    public class RepoProject : IProject
    {
        dpcpEntities dc;


        public RepoProject(dpcpEntities obj)
        {
            this.dc = obj;
        }
        public void Dispose()
        {
            //Win32.DestroyHandle(this.CursorFileBitmapIconServiceHandle);
        }

        public IEnumerable<Tbl_Project> getProjectName(string menu)
        {
            int id = get_Id_ByMenu(menu);
            var proname = (from n in dc.Tbl_Project where n.menu_Id == id orderby n.Priority select n).ToList();

            return proname;

        }

        public int get_Id_ByMenu(string menu)
        {
            var id = (from n in dc.Tbl_Menus where n.menu_Name == menu select n.menu_Id).FirstOrDefault();
            return id;
        }

        public IEnumerable<sp_GetProject_By_Menu_Result> getAll_ImagebyId(int id)
        {
            var result = dc.sp_GetProject_By_Menu(id).ToList();
            return result;
        }


        public IEnumerable<Tbl_Project_Img> getAll_Image()
        {
            var images = dc.Tbl_Project_Img.ToList();
            return images;
        }

        public IEnumerable<Tbl_Project_Img> getAll_ImageByMenu(string menu)
        {
            var images = (from i in dc.Tbl_Project_Img
                          join p in dc.Tbl_Project on 
                          i.Project_Id equals p.Project_Id 
                          where p.menu_Id == (from n in dc.Tbl_Menus where n.menu_Name == menu select n.menu_Id).FirstOrDefault() select i).ToList();
            return images;
        }

        public IEnumerable<sp_GetProject_By_Menu_Result> getproject_WithImagesBy_Menu(string menu)
        {
            int id = (from n in dc.Tbl_Menus where n.menu_Name == menu select n.menu_Id).FirstOrDefault();
            var result = dc.sp_GetProject_By_Menu(id).ToList();
            return result;
        }

        public void save()
        {
            dc.SaveChanges();
        }


    }
}