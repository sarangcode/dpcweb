﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPCWeb.Models;

namespace DPCWeb.Repository
{
    interface IHome:IDisposable
    {
        IEnumerable<Tbl_Banner> getallBanner();
        IEnumerable<tbl_Services> getallServices();
    }
}
