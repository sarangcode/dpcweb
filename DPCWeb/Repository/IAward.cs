﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPCWeb.Models;

namespace DPCWeb.Repository
{
    interface IAward:IDisposable
    {
        IEnumerable<Tbl_AwardImg> get_All_AwardImg();
    }
}
