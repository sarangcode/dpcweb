﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DPCWeb.Models;

namespace DPCWeb.Repository
{
    public class RepoAward:IAward
    {

        dpcpEntities dc;


        public RepoAward(dpcpEntities obj)
        {
            this.dc = obj;
        }
        public void Dispose()
        {
            //Win32.DestroyHandle(this.CursorFileBitmapIconServiceHandle);
        }
        public IEnumerable<Tbl_AwardImg> get_All_AwardImg()
        {
            var award = dc.Tbl_AwardImg.ToList();
            return award;
        }
    }
}